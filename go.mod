module gitlab.com/postmarketOS/postmarketos-mkinitfs

go 1.20

require (
	github.com/cavaliercoder/go-cpio v0.0.0-20180626203310-925f9528c45e
	github.com/klauspost/compress v1.15.12
	github.com/pierrec/lz4/v4 v4.1.17
	github.com/ulikunitz/xz v0.5.10
	golang.org/x/sys v0.18.0
)

require (
	github.com/mvdan/sh v2.6.4+incompatible // indirect
	golang.org/x/crypto v0.21.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/term v0.18.0 // indirect
	mvdan.cc/sh v2.6.4+incompatible // indirect
)
